(function () {
    'use strict';

    function headerTemplate2 (
        PATH_CONFIG,
        serviceImageCheck
    ) {
        return {
            restrict: 'EA',
            replace: true,

            link: function ($scope, element, attr) {

            },
            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                serviceImageCheck.IsImageHeaderLoaded('.background-image img', $scope);

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-header-template/header-template-2/header-template-2.html'
        }
    }

    headerTemplate2.$inject = [
        'PATH_CONFIG',
        'serviceImageCheck'
    ];

    angular
        .module('bravoureAngularApp')
        .directive('headerTemplate2', headerTemplate2);
})();

