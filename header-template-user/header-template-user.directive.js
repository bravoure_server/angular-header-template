(function () {
    'use strict';

    function headerTemplateUser (PATH_CONFIG) {
        return {
            restrict: 'EA',
            replace: true,

            controller: function ($scope, $controller) {

                angular.extend (this, $controller ('baseController', {
                    $scope: $scope
                }));

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-header-template/header-template-user/header-template-user.html'
        }
    }

    headerTemplateUser.$inject = ['PATH_CONFIG'];

    angular
        .module ('bravoureAngularApp')
        .directive ('headerTemplateUser', headerTemplateUser);
}) ();
