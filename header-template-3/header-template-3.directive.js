(function () {
    'use strict';

    function headerTemplate3 (
        PATH_CONFIG,
        serviceImageCheck
    ) {
        return {
            restrict: 'EA',
            replace: true,

            link: function (scope) {
            },

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                serviceImageCheck.IsImageHeaderLoaded('.background-image img', $scope);

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-header-template/header-template-3/header-template-3.html'
        }
    }

    headerTemplate3.$inject = [
        'PATH_CONFIG',
        'serviceImageCheck',
    ];

    angular
        .module('bravoureAngularApp')
        .directive('headerTemplate3', headerTemplate3);
})();
