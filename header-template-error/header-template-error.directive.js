'use strict';

function headerTemplateError(PATH_CONFIG) {
    return {
        restrict: 'EA',
        replace: true,

        controller: function ($scope, $controller, $timeout) {

            // Extend controller
            angular.extend(this, $controller('baseController', {
                $scope: $scope
            }));

        },

        templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-header-template/header-template-error/header-template-error.html'
    }
}

headerTemplateError.$inject = ['PATH_CONFIG'];

angular
    .module('bravoureAngularApp')
    .directive('headerTemplateError', headerTemplateError);
