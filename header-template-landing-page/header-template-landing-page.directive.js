(function () {

    'use strict';

    function headerTemplateLandingPage (
        PATH_CONFIG,
        $stateParams,
        serviceImageCheck
    ) {
        return {
            restrict: 'E',
            replace: true,

            link: function () {

            },

            controller: function ($scope, $controller) {

                // Extend controller
                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $scope.getSubbrandActiveClass = function (id, slug, subpage_id, subpage_slug) {
                    if (
                        $stateParams.slug == slug &&
                        $stateParams.id == id &&
                        $stateParams.subpage_id == subpage_id &&
                        $stateParams.subpage_slug == subpage_slug
                    ) {

                        return 'active';
                    }

                }

                serviceImageCheck.IsImageHeaderLoaded('.background-image img', $scope);

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-header-template/header-template-landing-page/header-template-landing-page.html'
        }
    }

    headerTemplateLandingPage.$inject = [
        'PATH_CONFIG',
        '$stateParams',
        'serviceImageCheck'
    ];

    angular
        .module('bravoureAngularApp')
        .directive('headerTemplateLandingPage', headerTemplateLandingPage);


})();
