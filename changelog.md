## Angular Block Header Template component / Bravoure component

This Component loads all the header templates for different templates, including error, landing pages & user.

### **Versions:**

## [1.1.1]
### Added
- Fixing minor bug in name of the directive

## [1.1.0]
### Added
- Adding show header functionality after the header iages is loaded
- blocks will be loaded after in the template

## [1.0.0]
### Added
- Initial version

---------------------
